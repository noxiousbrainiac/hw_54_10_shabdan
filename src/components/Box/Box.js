import React, {useState} from "react";
import './Box.css';

const Box = ({id, clickToShow, clicked, hasItem}) => {
    let hidden = ['hidden'];
    let symbol;

    if (clicked === true) {
        hidden.push('showBox');
    }

    if (hasItem === true) {
        symbol = 'O';
    }

    return (
        <div className={"box" + " " + id}>
            {symbol}
            <div
                key={id}
                onClick={clickToShow}
                className={hidden.join(' ')}
            >
            </div>
        </div>
    );
};

export default Box;