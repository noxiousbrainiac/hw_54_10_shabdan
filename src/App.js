import {useState} from "react";
import AppContainer from "./components/AppContainer/AppContainer";
import ResetButton from "./components/ResetButton/ResetButton";
import Counter from "./components/Counter/Counter";
import './components/Frameworks/bootstrap.min.css';

const App = () => {
    const emptyArray = [];

    const doArray = array => {
        const random = Math.floor(Math.random() * 35);
        for (let i = 0; i < 36; i++) {
            if (i === random){
                array.push({id: i, hasItem: true, clicked: false});
            } else {
                array.push({id: i, hasItem: false, clicked: false});
            }
        }
    };

    doArray(emptyArray);

    const [cubes, setCubes] = useState(emptyArray);
    const [count, setCount] = useState(0)

    const clickToShow = id => {
        setCubes(cubes.map(item => {
            if (item.id === id) {
                setCount(count + 1 );
                return {
                    ...item,
                    clicked: true,
                }
            }
            return  item;
        }));
    };

    const clickToReset = () => {
        setCubes(emptyArray);
        setCount(0);
    }

    return (
        <div className="container text-center">
            <AppContainer
                clickToShow={clickToShow}
                cubes={cubes}
            />
            <Counter tries={count}/>
            <ResetButton reset={clickToReset}/>
        </div>
    );
};

export default App;