import React from 'react';
import Box from "../Box/Box";
import '../Frameworks/bootstrap.min.css';

const AppContainer = ({cubes, showApp, clickToShow}) => {
    return (
        <div
            className="d-flex flex-wrap justify-content-around card m-5 p-1"
            style={{width: "400px", height: "400px"}}

        >
            {cubes.map((item, index)  => {
                return(
                    <Box
                        id={index}
                        key={index}
                        clickToShow={()=>clickToShow(index)}
                        showApp={showApp}
                        clicked={item.clicked}
                        hasItem={item.hasItem}
                    />
                )
            })}
        </div>
    );
};

export default AppContainer;