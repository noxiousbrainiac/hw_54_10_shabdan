import React from 'react';
import '../Frameworks/bootstrap.min.css'

const ResetButton = ({reset}) => {
    return (
        <button type="button" className="btn btn-primary" onClick={reset}>
            Reset
        </button>
    );
};

export default ResetButton;