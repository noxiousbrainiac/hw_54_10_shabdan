import React from 'react';

const Counter = ({tries}) => {
    return (
        <div>
            <p>
              Tries: {tries}
            </p>
        </div>
    );
};

export default Counter;